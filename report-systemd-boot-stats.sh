#!/bin/bash -e

declare -A PROPERTIES=(
    ["FirmwareTimestampMonotonic"]="ms"
    ["LoaderTimestampMonotonic"]="ms"
    ["KernelTimestampMonotonic"]="ms"
    ["InitRDTimestampMonotonic"]="ms"
    ["UserspaceTimestampMonotonic"]="ms"
    ["FinishTimestampMonotonic"]="ms"
)
function read_property {
    dbus-send --system --print-reply=literal \
        --dest=org.freedesktop.systemd1 \
        /org/freedesktop/systemd1 \
        org.freedesktop.DBus.Properties.Get \
        string:"org.freedesktop.systemd1.Manager" string:"$1" | awk '{print $3}'
}

for property in "${!PROPERTIES[@]}"
do
    val=$(read_property "$property")
    unit=${PROPERTIES[$property]}
    lava-test-case "systemd-$property" \
        --result pass \
        --measurement "$val" \
        --units "$unit"
done
